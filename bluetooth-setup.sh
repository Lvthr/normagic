#!/bin/bash
DEVICE_ID='60:C5:47:12:CE:0A'

# Connect to the keyboard over bluetooth
echo bluetootctl -- connect $DEVICE_ID
# Trust the keyboard. Setting the keyboard to trusted will ensure that the keyboard is connected automatically whenever it is turned on.
# When logging in to your system, if the keyboard is not already connected, press the "on button" on the keyboard and it should connect automatically.
echo bluetootctl -- trust $DEVICE_ID
