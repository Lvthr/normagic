# normagic

Setup script and files for setting up xkb layout with the Norwegian apple magic keyboard (model A1314) on X11 Linux systems. 

**Disclaimer: I have only tested this on Pop!_OS 20.10 x86_64 kernel 5.8.0-7625-generic with i3-gaps**
I *believe* the setup should work as long as it is run on a X11 Linux system, but cannot guarantee this as I have little experience with configuring X11/xkb. 

## Installation:
To perform the setup run the following commands:
```bash
git clone https://gitlab.com/Lvthr/normagic
cd normagic/
./setup.sh
```
The keyboard layout should now be setup every time you login.

Your keyboard should now function much like it does on a normal mac. Where the left `Cmd` and left `Ctrl` keys are switched. Such that `Cmd+c` will copy selected text, rather than the equivalent shortcut with the `Ctrl` key. 

**Important note**
This setup will change keycode 64 from registering as left Alt to ISO_Level3_Shift (by xev output), such that if you are using the i3 window manager, and have set your $mod key to be Mod1 (left Alt) you will have to set it to something else.
## Connect bluetooth keyboard on startup
Using `bluetoothctl` allows you to pair, connect, disconnect, ect. to bluetooth devices on Linux. 
You will first have to find the device ID of the device/keyboard you want to connect to. Turn off the device/keyboard, and run the following commands.
```bash
bluetoothctl
scan on
```
Turn on the device/keyboard and make it discoverable (by pressing some "bluetooth button" on the device, not nessecary if it is the apple magic keyboard because it does this every time it is turned on).
Wait until you see a device with the correct name pop up in the terminal window. The entry will have the form "Device AA:BB:CC:DD:EE:FF MyDevice Name", where the device ID is the "AA:BB...". 
When you have found the device ID, pair to the device as follows (still inside bluetoothctl program):
```bash
scan off
pair AA:BB:CC:DD:EE:FF
# Wait until device is paired
connect AA:BB:CC:DD:EE:FF
# Wait until device is connected
trust AA:BB:CC:DD:EE:FF
# This last command is important if you want your bluetooth keyboard to be connected on startup (before login)
# when you press the "on-button" on the keyboard/device. 
# If the device is not trusted by the system, the device will not be connected automatically every time it is turned on. 
```
### Setup script description
The `setup.sh` script will copy the keyboard configuration files in this repo to their correct locations inside `/usr/share/X11/xkb/`, such that the keyboard layout is selectable as a normal keyboard layout using systems settings managers (such as `gnome-control-center`). It will also select this newly added keyboard layout and update the currently used keyboard layout, such that after running the setup script your system should be using this layout. An easy check is to press the `@` key (to the right of `æ` on the home row) to see if the `@` symbol is posted, if it is not the configuration has not been setup as expected. 
Furter, to check that `lAlt` is working as expected, press `lAlt+8` and/or `lAlt+9`. In this case left and right square brackets (these: `[` and `]`) should be posted.
The setup script will also create a directory called `normagic` at `$HOME/.config/normagic/`, and copy the `Xmodmap` file from this repo into that directory.
It will also copy the `.xinitrc` from this repo directly to your home directory (`$HOME`), such that the `$HOME/.config/normagic/Xmodmap` is automatically run on startup.
When running the setup script, you might have to provide sudo password.

Short version of what the script does in order:
```bash
cp ./Xmodmap $HOME/.config/normagic/
sudo cp ./normagic /usr/share/X11/xkb/symbols/normagic
sudo cp ./evdev.xml /usr/share/X11/xkb/rules/evdev.xml
sudo cp ./xorg.lst /usr/share/X11/xkb/rules/xorg.lst
sudo dpkg-reconfigure xkb-data
setxkbmap -layout normagic -model macbook79
xmodmap ./Xmodmap
cp ./.xinitrc $HOME/
```

**A comment on the `keyboard` file in this repo**
The `keyboard` file in this repo contains the file used by X11/xkb to get the default keyboard configuration on startup (I think).
A file that is equal to this file should be created automatically at `/etc/default/keyboard` (except for the top comment in the file in this repo) when running the setup script, and you should not need to use it. 
If such a file is not created automatically at the above location, you can copy it manually. You might have to remove the comment first. Logout and back in and the file should be used. 

## Useful references
Most of the information here I have found from the following resources:

http://people.uleth.ca/~daniel.odonnell/Blog/custom-keyboard-in-linuxx11
https://github.com/gulrotkake/Ubuntu-MacBook-Norwegian-keyboard/blob/master/no
https://www.x.org/releases/X11R7.5/doc/input/XKB-Enhancing.html
