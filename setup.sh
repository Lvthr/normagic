#!/bin/bash

# This is the setup script for the normagic keyboard layout 
# for X11 systems/systems using xkb for keyboard layouts.
echo "Setting up the normagic keyboard configuration"
echo ""

CONFIG_DIRECTORY="$HOME/.config/normagic/"
FILENAME="Xmodmap"

placexmodmap() {
    ORIGIN=$PWD/$FILENAME
    echo "Copying $ORIGIN to $CONFIG_DIRECTORY"
    cp $ORIGIN $CONFIG_DIRECTORY
}


if [[ -d "$CONFIG_DIRECTORY" ]]; then
    if [[ -f "$CONFIG_DIRECTORY/$FILENAME" ]]; then
        # Directory and file exists, check if user wants to overwrite current file
        echo "There already exists an Xmodmap file at $CONFIG_DIRECTORY"
        echo -n "Do you want to overwrite the file? [y/n]: "
        read ans
        if [[ $ans == "y" ]]; then
            placexmodmap
        elif [[ $ans == "n" ]]; then
            echo "Skipping Xmodmap script copy to $CONFIG_DIRECTORY"
        else
            echo "unknown command \"$ans\", exiting without completion."
            exit 1
        fi
    else
        # Directory exists, but file does not. Copy the Xmodmap file to the directory
        placexmodmap
    fi
else
    echo "Creating $CONFIG_DIRECTORY"
    mkdir $CONFIG_DIRECTORY
    placexmodmap
fi
echo ""

# Place the keyboard layout files in the correct location
# This is really only required to do once, as they are persistent
# But there is no harm (I think) in doing it more than once
ERROR=0
sudo cp $PWD/normagic /usr/share/X11/xkb/symbols/normagic
if [[ $? -ne 0 ]]; then
    # echo "An error occured: could not copy file $PWD/normagic to /usr/share/X11/xkb/symbols/normagic"
    ERROR=1
fi

sudo cp $PWD/evdev.xml /usr/share/X11/xkb/rules/evdev.xml
if [[ $? -ne 0 ]]; then
    # echo "An error occured: could not copy file $PWD/evdev.xml to /usr/share/X11/xkb/rules/evdev.xml"
    ERROR=1
fi

sudo cp $PWD/xorg.lst /usr/share/X11/xkb/rules/xorg.lst
if [[ $? -ne 0 ]]; then
    #echo "An error occured: could not copy file $PWD/xorg.lst_test to /usr/share/X11/xkb/rules/xorg.lst"
    ERROR=1
fi


# Force X11/xkb to reconfigure using the new layout files
# I'm still not quite shure what this command does "under the hood", nor wether it is strictly nessecary. 
# Havent had any problems including/excluding it yet, so I keep it here (at least for future reference)
sudo dpkg-reconfigure xkb-data
if [[ $? -ne 0 ]]; then
    #echo "An error occured: could not force xkb to reconfigure using the new layoyt files"
    ERROR=1
fi
# Set the X11/xkb keboard mapping: set layout to the one we just added to X11/xkb/symbols (and the other files) 
# and set the keyboard model to the norwegian macbook keyboard model
setxkbmap -layout normagic -model macbook79
if [[ $? -ne 0 ]]; then
    #echo "An error occured: could not set xkb layout and model correctly"
    ERROR=1
fi
# On MacOS the Cmd key does almost everything the Ctrl key would usually do, such as Cmd+c would copy text/content on a mac
# when this is usually done with Ctrl+c on most linux and Windows. 
# I am more comfortable using the mac keybindings, so to replicate this functionality we must use xmodmap to swap the function of these keys. 
# Usually left Ctrl would function as Control_L and left (and right?) Cmd would function as Super_L
# Swap left Ctrl and left Cmd functions using xmodmap. 
xmodmap $PWD/Xmodmap
if [[ $? -ne 0 ]]; then
    # echo "An error occured: could not run Xmodmap script"
    ERROR=1
fi

# Copy the .xinitrc file to the home directory. This will replace any file with the same name already in this directory. 
if [[ -f $HOME/.xinitrc ]]; then
    echo "There already exists a .xinitrc file at $HOME"
    echo -n "Do you want to overwrite it? [y/n]: "
    read ans
    if [[ $ans == "y" ]]; then
        echo "Copying $PWD/.xinitrc to $HOME/.xinitrc"
        cp $PWD/.xinitrc $HOME/
        if [[ $? -ne 0 ]]; then
            #echo "An error occured: could not copy $PWD/.xinitrc to $HOME"
            ERROR=1
        fi
    elif [[ $ans == "n" ]]; then
        echo "Skipping .xinitrc configuration copy to $HOME"
    else
        echo "unknown command \"$ans\", exiting without completion."
        exit 1
    fi
else
    echo "Copying $PWD/.xinitrc to $HOME/.xinitrc"
    cp $PWD/.xinitrc $HOME/
    if [[ $? -ne 0 ]]; then
        #echo "An error occured: could not copy $PWD/.xinitrc to $HOME"
        ERROR=1
    fi
fi

echo ""
if [[ $ERROR -eq 0 ]]; then
    echo "Keyboard configuration added and updated successfully"
    exit 0
else
    echo "Some error(s) occured during setup. Keyboard configuration was unsuccessful."
    exit 1
fi
