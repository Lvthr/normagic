#!/bin/bash
sudo cp ./masterfiles/no.master /usr/share/X11/xkb/symbols/no
sudo cp ./masterfiles/evdev.xml.master /usr/share/X11/xkb/rules/evdev.xml
sudo cp ./masterfiles/xorg.lst.master /usr/share/X11/xkb/rules/xorg.lst
sudo dpkg-reconfigure xkb-data
echo "Reset the keyboard layout to with masterfiles"