# Norwegian Apple Magic keyboard on Linux/X11
Most of the information here I have found here:

http://people.uleth.ca/~daniel.odonnell/Blog/custom-keyboard-in-linuxx11

key <AB09>
    : A - The alphanumeric key block
    : B - The second row from the bottom (A = spacebar row, B = zxc.. row, c = asd.. row)
    : 09 - The ninth key from the left (exluding Shift and "<>" keys)