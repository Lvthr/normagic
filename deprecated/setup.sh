#!/bin/bash

# Disable readline arguments in terminal (alt + number displaying arg: n)
bind -r '\e-'
bind -r '\e0'
bind -r '\e1'
bind -r '\e2'
bind -r '\e3'
bind -r '\e4'
bind -r '\e5'
bind -r '\e6'
bind -r '\e7'
bind -r '\e8'
bind -r '\e9'

sudo cp ./normagic /usr/share/X11/xkb/symbols/normagic
sudo cp ./evdev.xml /usr/share/X11/xkb/rules/evdev.xml
sudo cp ./xorg.lst /usr/share/X11/xkb/rules/xorg.lst
sudo dpkg-reconfigure xkb-data
setxkbmap -layout normagic
# Swap lCtrl and lCmd
xmodmap ./Xmodmap
echo "Added the new keyboard layouts"
